#
# Authors: 
# Corrado Maurini <corrado.maurini@upmc.fr>
# Jack S. Hale <jack.hale@uni.lu>
# Vahid Ziaei-Rad <vahid.ziaei@uni.lu>

import math
import os
import sys

import numpy as np
import sympy
import pandas as pd

from dolfin import *
from mshr import *

# Setup and parameters
set_log_level(INFO)

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["quadrature_degree"] = 1
parameters["form_compiler"]["cpp_optimize"] = True

# Parameters of the nonlinear solver used for the damage problem
snes_solver_parameters_bounds = {
 'nonlinear_solver': 'snes',
 'snes_solver': {'absolute_tolerance': 1.0e-8,
                 'line_search': 'basic',
                 'linear_solver': 'mumps',
                 'lu_solver': {'reuse_factorization': False},
                 'maximum_iterations': 50,
                 'method': 'vinewtonrsls',
                 'relative_tolerance': 1.0e-8,
                 'report': False},
 'symmetric': True}

# Geometry
L = 4.0 # length
H = 2.0 # height
cell_size = 0.044 # target cell size
L_c = 0.5 * L # initial notch crack length

# Material constants
ell_v = 2 * cell_size
ell = Constant(ell_v) # internal length scale
E = 10.0 # Young modulus
nu = 0.2 # Poisson ratio
lmbda = E * nu / (1.0 - nu ** 2) # Lame constant
mu = E / (2.0 * (1.0 + nu)) # shear modulus

Gc = Constant(1.0) # fracture toughness
k_ell = Constant(1.0e-8) # residual stiffness
# k_ell = cell_size # residual stiffness (Vahid: Seems strange)

# Loading
ut = 1.0 # maximum loading 
load_min = 0.0 # load multiplier min value
load_max = 1.3 # load multiplier max value
load_steps = 20 # number of time steps

# Stopping criteria for the alternate minimization
max_iterations = 200 
tolerance = 1.0e-8

# Constitutive functions of the damage model
def w(alpha):
    return alpha*2

def a(alpha):
    return (1 - alpha)**2

# others
prefix = "L%s-H%s-S%s" % (L, H, cell_size)
save_dir = "results/" + prefix + "/"

# Unstructured mesh generation with mshr/CGAL.
mesh = Mesh()
geom = Rectangle(Point(0., 0.), Point(L, H))
mesh_generator = CSGCGALMeshGenerator2D() 
mesh_generator.parameters["mesh_resolution"] = -1.0
mesh_generator.parameters["cell_size"] = cell_size
mesh_generator.generate(geom, mesh)

# Normalization constant for the dissipated energy 
# to get Griffith surface energy for ell going to zero
z = sympy.Symbol("z")
c_w = 2.0*sqrt(2.0)*sympy.integrate(sympy.sqrt(w(z)), (z, 0.0, 1.0))

# Define boundary sets for boundary conditions
class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 2.0, eps=100.0*DOLFIN_EPS) and on_boundary

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0, eps=100.0*DOLFIN_EPS) and x[0] > L_c and on_boundary

# Initialize sub-domain instances
top = Top() 
bottom = Bottom()

# define MeshFunction to identify boundaries by numbers
boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)
top.mark(boundaries, 1) # mark top as 1
bottom.mark(boundaries, 2) # mark bottom as 2
# Define new surface measure including boundary marking 
ds = Measure("ds", subdomain_data=boundaries) # top: ds(1), bottom: ds(2)

# Variational formulation 

# Create function space for 2D elasticity + Damage
V_u = VectorFunctionSpace(mesh, "CG", 1)
V_alpha = FunctionSpace(mesh, "CG", 1)

# Define the function, test and trial fields
u_, u, u_t = Function(V_u), TrialFunction(V_u), TestFunction(V_u)
alpha_, alpha, alpha_t = Function(V_alpha), TrialFunction(V_alpha), TestFunction(V_alpha)

alpha_0 = interpolate(Constant(0.0), V_alpha) # initial (known) alpha, undamaged everywhere.

# Dirichlet boundary condition for a traction test boundary
ndim  = 2
u_T = Expression(("0.", "t",), t=0.0, degree=1)
u_B = Constant((0.,) * ndim)
# bc - u (imposed displacement)
Gamma_u_0 = DirichletBC(V_u, u_T, boundaries, 1)
Gamma_u_1 = DirichletBC(V_u, u_B, boundaries, 2)
bc_u = [Gamma_u_0, Gamma_u_1]

# bc - alpha (zero damage)
Gamma_alpha_0 = DirichletBC(V_alpha, 0.0, boundaries, 1)
bc_alpha = [Gamma_alpha_0]

def eps(u_):
    return sym(grad(u_))

def sigma_0(u_):
    return 2.0*mu*(eps(u_)) + lmbda*tr(eps(u_))*Identity(ndim)

def sigma(u_, alpha_):
    return (a(alpha_) + k_ell)*sigma_0(u_)

elastic_energy = 0.5*inner(sigma(u_,alpha_), eps(u_))*dx
# elastic_energy = 0.5*mu*(a(alpha_) + k_ell)*dot(grad(u_), grad(u_))*dx
dissipated_energy = (Gc/float(c_w))*((w(alpha_)/ell) + 0.5*ell*dot(grad(alpha_), grad(alpha_)))*dx 
total_energy = elastic_energy + dissipated_energy

# Residual and Jacobian of elasticity problem
F_u = derivative(total_energy, u_, u_t)
J_e = derivative(F_u, u_, u) 
# Residual and Jacobian of damage problem
F_d = derivative(total_energy, alpha_, alpha_t)
J_d = derivative(F_d, alpha_, alpha)

# Variational problem for the displacement
# TODO: Could be more natural to express this as a NonlinearVariationalProblem as well?
problem_u = LinearVariationalProblem(lhs(J_e), rhs(F_u), u_, bc_u)

# Nonlinear variational problem for the damage variable
# Lower bound on damage, will be updated to enforce irreversibility during
# the alternate minimisation scheme. 
lower_bound = interpolate(Constant(0.0), V_alpha) 
upper_bound = interpolate(Constant(1.0), V_alpha)

problem_alpha_nl = NonlinearVariationalProblem(F_d, alpha_, bc_alpha, J=J_d)
problem_alpha_nl.set_bounds(lower_bound, upper_bound)

# Set up the solvers                                        
solver_u = LinearVariationalSolver(problem_u)
solver_alpha = NonlinearVariationalSolver(problem_alpha_nl)     
solver_alpha.parameters.update(snes_solver_parameters_bounds)
# info(solver_alpha.parameters,True) # uncomment to see available parameters

# To store results
results = []
file_alpha = XDMFFile(save_dir + "alpha.xdmf")
file_u = XDMFFile(save_dir + "u.xdmf")

# Solving at each timestep
load_multipliers = np.linspace(load_min, load_max, load_steps)
for (i_t, t) in enumerate(load_multipliers):
    u_T.t = t*ut
    # Alternate mininimization
    # Initialization
    iteration = 0 
    err_alpha = 1.0
    # Iterations
    while err_alpha > tolerance and iteration < max_iterations:
        # solve elastic problem
        solver_u.solve()
        # solve damage problem
        solver_alpha.solve()
        # test error
        err_alpha = (alpha_.vector() - alpha_0.vector()).norm('linf')
        # monitor the results
        if mpi_comm_world().rank == 0:
            print "Iteration: %i, Error: %2.8s" % (iteration, err_alpha)
        # update iteration
        alpha_0.vector()[:] = alpha_.vector()
        iteration += 1
    
    # Update the lower bound to account for the irreversibility
    lower_bound.vector()[:] = alpha_.vector()
    
    # Calculate the energies
    elastic_energy_value = assemble(elastic_energy)
    surface_energy_value = assemble(dissipated_energy)
    
    if mpi_comm_world().rank == 0:
        print "End of timestep %d with load multiplier %g" % (i_t, t)
        print "Elastic and surface energies: (%g, %g)" % (elastic_energy_value, surface_energy_value)
        print ""
        print ""
   
    result = {"timestep": t,
              "displacement" : u_T.t,
              "elastic_energy": elastic_energy_value,
              "surface_energy": surface_energy_value,
              "total_energy": surface_energy_value + elastic_energy_value}
    results.append(result)
     
    # Write out solution at current timestep to file 
    file_alpha.write(alpha_, t) 
    file_u.write(u_, t) 

df = pd.DataFrame(results)
print results
df.to_json(save_dir + "results.json")

# Plot energy and stresses
import matplotlib.pyplot as plt

def plot_energy(df):
    p1, = plt.plot(df["displacement"], df["elastic_energy"], 'b-o', linewidth=2)
    p2, = plt.plot(df["displacement"], df["surface_energy"], 'r-o', linewidth=2)
    p3, = plt.plot(df["displacement"], df["total_energy"], 'k--', linewidth=2)
    
    plt.legend([p1, p2, p3], ["Elastic", "Dissipated", "Total"])
    plt.xlabel('Displacement')
    plt.ylabel('Energies')

plot_energy(df)
plt.savefig(save_dir + 'energies.png')
