# FEnics code  Variational Fracture Mechanics
#
# A static solution of the variational fracture mechanics problems using the regularization AT2
#
# author: corrado.maurini@upmc.fr
#
# date: 25/05/2013
#
# install readline pyzmq sphinx pygments
from dolfin import *
from mshr import *
import sys, os, sympy, math
import numpy as np

parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"

# ------------------
# Parameters
# ------------------
set_log_level(INFO)  # log level
parameters.parse()  # read paramaters from command line

solver_alpha_parameters = {"method": "gpcg",
                           "linear_solver": "cg",
                           "preconditioner": "bjacobi",
                           "report": False}
solver_u_parameters = {"linear_solver": "cg",
                       "symmetric": True,
                       "preconditioner": "hypre_amg",
                       "krylov_solver": {
                           "report": False,
                           "monitor_convergence": False,
                           "relative_tolerance": 1e-8
                       }
                       }

# Geometry and Material constants
L = 4.;
H = 4.;

cell_size = .044 #From Ex.1(after refinement: h = 0.022, 0.011)

E, nu = Constant(1.0), Constant(0.2) #From p. 8
mu = E / (2.0 * (1.0 + nu))
lmbda = E * nu / (1.0 - nu ** 2)
Gc = Constant(1.0) #From Wheeler 2014

#ell = Constant(ellv)
#k_ell = Constant(1.e-16)  # residual stiffness

#Special paramenters according to case 4, (p.8) (h<<k<<ell) k = 0.25 * sqrt(h), ell = 0.5 * sqrt(k)
k_ell = Constant(sqrt(cell_size)/4.)
ellv = 0.5 * sqrt(k_ell)
ell = Constant(ellv)

# Loading
ut = 2.  # In this problem, pressure is used to drive the displacememnt and the crack propagation
#f = 0.  # bulk load

# Numerical parameters of the alternate minimization
maxiter = 1000 #We use a loosely-coupled scheme in which we only solve once per subproblem and then proceed to the next time step
toll = 1e-5 #We run the subproblem until the following tolerance appears: \u^{n-1} - u^n\<toll

# Constitutive functions of the damage model
def w(alpha):
    return alpha


def a(alpha):
    return (1 - alpha) ** 2


modelname = "model-AT1"
# others
meshname = "mesh/bar-L%s-H%.2f-S%.4f.xml" % (L, H, cell_size)
savedir = "results/%s-bar-L%s-H%.2f-S%.4f-l%.4f" % (modelname, L, H, cell_size, ellv)

# ------------------
# Geometry and mesh generation
# ------------------
geom = Rectangle(Point(0., 0.), Point(L, H))
# hole=Circle(L/2.,H/2.,H/4.) # uncomment to add a hole
# geom=geom-hole # uncomment to add a hole
mesh = Mesh()
mesh_generator = CSGCGALMeshGenerator2D()
mesh_generator.parameters["mesh_resolution"] = -1.  #
mesh_generator.parameters["cell_size"] = cell_size  #
mesh_generator.generate(geom, mesh)
plot(mesh)
# mesh = Mesh("meshSingle.xml")
ndim = 2  # get number of space dimensions

# -------------------
# Useful definitions
# -------------------
# zero and unit vectors
zero_v = Constant((0.,) * ndim)

# Strain and stress
def eps(v):
    return sym(grad(v))

def sigma_0(v):
    return 2.0 * mu * (eps(v)) + lmbda * tr(eps(v)) * Identity(ndim)

#Added by Vahid: \psi_{Dev} needs to be added
def sigma(v, alpha):
    return (a(alpha) + k_ell) * sigma_0(v)

# Normalization constant for the dissipated energy
# to get Griffith surface energy for ell going to zero
# z = sympy.Symbol("z")
# c_w = 2 * sqrt(2) * sympy.integrate(sympy.sqrt(w(z)), (z, 0, 1))

# body force
body_force = Constant((0., 0.))

# ----------------------------------------------------------------------------
# Define boundary sets for boundary conditions
# ----------------------------------------------------------------------------
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0] * 0.01, 0)

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near((x[0] - L) * 0.01, 0.)

#Added by Vahid
class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1] * 0.01, 0)

class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near((x[1] - H) * 0.01, 0.)


# Initialize sub-domain instances
left = Left()
right = Right()

#Added by Vahid
bottom = Bottom()
top = Top()

# define meshfunction to identify boundaries by numbers
boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)

#Commented by Vahid
#left.mark(boundaries, 1)  # mark left as 1
#right.mark(boundaries, 2)  # mark right as 2
#Added by Vahid
#bottom.mark(boundaries, 3)  # mark left as 1
#top.mark(boundaries, 4)  # mark right as 2

# Define new measure including boundary naming
ds = Measure("ds")(subdomain_data=boundaries)  # left: ds(1), right: ds(2)

# ----------------------------------------------------------------------------
# Variational formulation
# ----------------------------------------------------------------------------
# Create function space for 2D elasticity + Damage
V_u = VectorFunctionSpace(mesh, "CG", 1)
#V_p = FunctionSpace(mesh, "CG", 1)
V_alpha = FunctionSpace(mesh, "CG", 1)

#Added by Vahid/ mixed function spaces
#V_m = MixedFunctionSpace([V_u, V_p])
#m_, m, m_t = Function(V_m), TrialFunction(V_m), TestFunction(V_m)
#u_, p_ = m_.split()

u_, u, u_t = Function(V_u), TrialFunction(V_u), TestFunction(V_u)
alpha_, alpha, alpha_t = Function(V_alpha), TrialFunction(V_alpha), TestFunction(V_alpha)

#The injected pressure
#p_b = 1.0e-3
#p_b = interpolate(Expression("1.0e-3"), V_alpha)

elastic_energy = inner(sigma(u_, alpha_), eps(u_)) * dx
external_work = dot(body_force, u_) * dx
dissipated_energy = Gc * 0.5 * ((w(alpha_) * w(alpha_)) / ell + ell * dot(grad(alpha_), grad(alpha_))) * dx

#Added by Vahid
#vol_energy_i = (alpha_ * alpha_) * p_b * div(u_) * dx
#vol_energy_ii = inner((alpha_ * alpha_) * grad(p_b), u_) * dx
#vol_energy = vol_energy_i + vol_energy_ii

total_energy = elastic_energy + dissipated_energy - external_work

# Derivatives of the energy
E_u = derivative(total_energy, u_, u_t)
E_du = replace(E_u, {u_: u})  # Writing tangent problems in term of test and trial functions for matrix assembly

E_alpha = derivative(total_energy, alpha_, alpha_t)
E_alpha_alpha = derivative(E_alpha, alpha_, alpha)

problem_u = LinearVariationalProblem(lhs(E_du), rhs(E_du), u_)


#Added by Vahid for initial phase field
class MyExpression(Expression):
    def eval(self, value, x):
        #Commented by Vahid for test
        #dx = x[0] - 0.5
        #Added by Vahid for test: two lines
        dx = x[0] - 1.8
        dz = x[0] - 2.2
        dy = x[1] - 2.0
        ell = ellv
        # if dx < 0.0
        if dx < 0.0 or dz > 0.0:
            #Added by Vahid for test: one line
            dx = min(abs(dx), abs(dz))

            value[0] = exp(-sqrt(dx * dx + dy * dy) / ell)
        else:
            value[0] = exp(-abs(dy) / ell)

            def value_shape(self):
                return (1,)

fd = MyExpression(degree=1)
alpha_0 = interpolate(fd, V_alpha)
alpha_.assign(alpha_0)

# Variational problem for the damage (non-linear to use variational inequality solvers of petsc)
class DamageProblem(OptimisationProblem):
    def f(self, x):
        return assemble(total_energy)

    def F(self, b, x):
        pass

    def J(self, A, x):
        pass

    def form(self, A, b, x):
        # alpha.vector()[:] = x
        assemble_system(E_alpha_alpha, E_alpha, bc_alpha, A_tensor=A, b_tensor=b)


problem_alpha = DamageProblem()

# Set up the solvers
solver_u = LinearVariationalSolver(problem_u)
solver_u.parameters.update(solver_u_parameters)
# info(solver_u.parameters, True)
solver_alpha = PETScTAOSolver()
solver_alpha.parameters.update(solver_alpha_parameters)
lb = interpolate(Expression("0."), V_alpha)  # lower bound, set to 0
ub = interpolate(Expression("1."), V_alpha)  # upper bound, set to 1
# info(solver_alpha.parameters,True) # uncomment to see available parameters

#  initialization of structures to store datas
errors = [];
alpha_max = [];
file_alpha = File(savedir + "/alpha.xdmf")  # use .pvd if .xdmf in not working
file_u = File(savedir + "/u.xdmf")  # use .pvd if .xdmf in not working

# Alternate mininimization
# Initialization
iter = 1;
err_alpha = 1

#Commented by Vahid for initializing phase field
#alpha_0 = interpolate(Expression("0.0"), V_alpha)  # initial (known) alpha

# Iterations
while err_alpha > toll and iter < maxiter:
    # solve elastic problem
    #Commented by Vahid: BCH
    solver_u.solve()

    # solve damage problem
    solver_alpha.solve(problem_alpha, alpha_.vector(), lb.vector(), ub.vector())
    # test error
    alphadiff = alpha_.vector().array() - alpha_0.vector().array()
    err_alpha = np.linalg.norm(alphadiff, ord=np.Inf)
    # monitor the results
    if mpi_comm_world().rank == 0:
        print "Iteration:  %2d, Error: %2.8s, alpha_max: %.8g" % (iter, err_alpha, alpha_.vector().max())
    # update iteration
    alpha_0.assign(alpha_)
    iter = iter + 1
    # plot the damage fied
    plot(alpha_)
    # Save the field at each iteration
    file_alpha << (alpha_)
    file_u << (u_)
    # save the error
    errors.append(err_alpha)
    alpha_max.append(alpha_.vector().max())

# Calculate the energies
elastic_energy_value = assemble(elastic_energy)
surface_energy_value = assemble(dissipated_energy)
if mpi_comm_world().rank == 0:
    print "-----------------------------------------"
    print("\nElastic and surface energies: (%g,%g)" % (elastic_energy_value, surface_energy_value))
    print "-----------------------------------------"

# Save some global quantities as a function of the time
# np.savetxt(savedir+'/errors.txt', iterations)

# Plot the damage field and the mesh
plot(alpha_)
list_timings()

# Plot energy and stresses
import matplotlib.pyplot as plt


def plot_errors():
    plt.semilogy(errors[:], 'b-o', linewidth=2)
    plt.xlabel('Iteration')
    plt.ylabel('Error')


def plot_alpha_max():
    plt.plot(alpha_max[:], 'b-o', linewidth=2)
    plt.xlabel('Iteration')
    plt.ylabel('alpha_max')


def plot_errors_alpha_max():
    plt.subplot(211)
    plot_errors()
    plt.subplot(212)
    plot_alpha_max()
    plt.savefig(savedir + '/iterations.png')
    plt.show()


# plot the errors
plot_errors_alpha_max()
# make plots interactive
interactive()
