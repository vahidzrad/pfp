# FEnics code  Variational Fracture Mechanics
#
# A static solution of the variational fracture mechanics problems using the regularization AT2
#
# author: corrado.maurini@upmc.fr
#
# date: 25/05/2013
#
# install readline pyzmq sphinx pygments
from dolfin import *
from mshr import *
import sys, os, sympy, math
import numpy as np

parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"

# ------------------
# Parameters
# ------------------
set_log_level(INFO)  # log level
parameters.parse()  # read paramaters from command line
solver_alpha_parameters = {"method": "gpcg",
                           "linear_solver": "cg",
                           "preconditioner": "bjacobi",
                           "report": False}
solver_u_parameters = {"linear_solver": "cg",
                       "symmetric": True,
                       "preconditioner": "hypre_amg",
                       "krylov_solver": {
                           "report": False,
                           "monitor_convergence": False,
                           "relative_tolerance": 1e-8
                       }
                       }

# Geometry and Material constants
L = 1.;
H = 1.;
ellv = 0.2 / (sqrt(2));
cell_size = .02
E, nu = Constant(10.0), Constant(0.3)
#Added by Vahid
mu = E / (2.0 * (1.0 + nu))
lmbda = E * nu / (1.0 - nu ** 2)

Gc = Constant(1.0)
ell = Constant(ellv)
k_ell = Constant(1.e-16)  # residual stiffness
# Loading
ut = 2.  # reference value for the loading (imposed displacement)
#f = 0.  # bulk load
# Numerical parameters of the alternate minimization
maxiter = 1000
toll = 1e-8


# Constitutive functions of the damage model
def w(alpha):
    return alpha


def a(alpha):
    return (1 - alpha) ** 2


modelname = "model-AT1"
# others
meshname = "mesh/bar-L%s-H%.2f-S%.4f.xml" % (L, H, cell_size)
savedir = "results/%s-bar-L%s-H%.2f-S%.4f-l%.4f" % (modelname, L, H, cell_size, ellv)

# ------------------
# Geometry and mesh generation
# ------------------
# geom = Rectangle(Point(0., 0.), Point(L, H))
# # hole=Circle(L/2.,H/2.,H/4.) # uncomment to add a hole
# # geom=geom-hole # uncomment to add a hole
# mesh = Mesh()
# mesh_generator = CSGCGALMeshGenerator2D()
# mesh_generator.parameters["mesh_resolution"] = -1.  #
# mesh_generator.parameters["cell_size"] = cell_size  #
# mesh_generator.generate(geom, mesh)
# plot(mesh)
mesh = Mesh("meshSingle.xml")

ndim = 2  # get number of space dimensions

# -------------------
# Useful definitions
# -------------------
# zero and unit vectors
zero_v = Constant((0.,) * ndim)
e1 = [Constant([1., 0.]), Constant((1., 0., 0.))][ndim - 2]


# Strain and stress
def eps(v):
    return sym(grad(v))


def sigma_0(v):
    #mu = E / (2.0 * (1.0 + nu))
    #lmbda = E * nu / (1.0 - nu ** 2)
    return 2.0 * mu * (eps(v)) + lmbda * tr(eps(v)) * Identity(ndim)


# Normalization constant for the dissipated energy
# to get Griffith surface energy for ell going to zero
z = sympy.Symbol("z")
c_w = 2 * sqrt(2) * sympy.integrate(sympy.sqrt(w(z)), (z, 0, 1))

# body force
body_force = Constant((0., 0.))


# ----------------------------------------------------------------------------
# Define boundary sets for boundary conditions
# ----------------------------------------------------------------------------
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0] * 0.01, 0)

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near((x[0] - L) * 0.01, 0.)

#Added by Vahid
class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1] * 0.01, 0)

class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near((x[1] - H) * 0.01, 0.)


# Initialize sub-domain instances
left = Left()
right = Right()

#Added by Vahid
bottom = Bottom()
top = Top()

# define meshfunction to identify boundaries by numbers
boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)

#Commented by Vahid
#left.mark(boundaries, 1)  # mark left as 1
#right.mark(boundaries, 2)  # mark right as 2

#Added by Vahid
bottom.mark(boundaries, 3)  # mark left as 1
top.mark(boundaries, 4)  # mark right as 2

# Define new measure including boundary naming
ds = Measure("ds")[boundaries]  # left: ds(1), right: ds(2)

# ----------------------------------------------------------------------------
# Variational formulation
# ----------------------------------------------------------------------------
#Added by Vahid
def STOKES(mesh):
    U = VectorFunctionSpace(mesh, "Lagrange", 1, dim=2)
    P = FunctionSpace(mesh, "Lagrange", 1)
    #D = FunctionSpace(mesh, "Lagrange", 1)
    #UP = MixedFunctionSpace([U, P])
    return (U,P)
#V_u, V_p = STOKES(mesh)
#UP = MixedFunctionSpace([U, P])
#V_up = Function(UP)
#Added/Commented by Vahid
#V_u, V_p = split(V_up)

#Added by Vahid
V_u, V_p = STOKES(mesh)
V_up = MixedFunctionSpace([V_u, V_p])
#V_u, V_p = split(V_up)

# Create function space for 2D elasticity + Damage
# Commented by Vahid
#V_u = VectorFunctionSpace(mesh, "CG", 1)
V_alpha = FunctionSpace(mesh, "CG", 1)
#Added/Commented by Vahid for incompressibility
#V_p = FunctionSpace(mesh, "CG", 1)

# Define the function, test and trial fields
u, du, v = Function(V_u), TrialFunction(V_u), TestFunction(V_u)
#Added by Vahid
up, dup, vp = Function(V_up), TrialFunction(V_up), TestFunction(V_up)

alpha, dalpha, beta = Function(V_alpha), TrialFunction(V_alpha), TestFunction(V_alpha)
#Added by Vahid
#k = lmbda + 2.0/ndim * mu
p = Function(V_p)
#energy_vol = (p * div(u) - (p*p)/(2*k)) * dx(domain=mesh)
energy_vol = (p * div(u) - (p*p)/(2*lmbda)) * dx

#Commented by Vahid
# Dirichlet boundary condition for a traction test boundary
#Commented by Vahid
u_L = zero_v
u_R = Expression(("t", "0.",), t=ut)

#Added by Vahid
u_B = zero_v
#u_B = Expression(("0.", "t",), t=-ut)
u_T = Expression(("0.", "t",), t=ut)

# bc - u (imposed displacement)
#Commented by Vahid
#Gamma_u_0 = DirichletBC(V_u, u_L, boundaries, 1)
#Gamma_u_1 = DirichletBC(V_u, u_R, boundaries, 2)

#Added by Vahid
Gamma_u_0 = DirichletBC(V_u, u_B, boundaries, 3)
Gamma_u_1 = DirichletBC(V_u, u_T, boundaries, 4)

bc_u = [Gamma_u_0, Gamma_u_1]
# bc - alpha (zero damage)
#Commented by Vahid
#Gamma_alpha_0 = DirichletBC(V_alpha, 0.0, boundaries, 1)
#Gamma_alpha_1 = DirichletBC(V_alpha, 0.0, boundaries, 2)

#Added by Vahid
Gamma_alpha_0 = DirichletBC(V_alpha, 0.0, boundaries, 3)
Gamma_alpha_1 = DirichletBC(V_alpha, 0.0, boundaries, 4)

bc_alpha = [Gamma_alpha_0, Gamma_alpha_1]


# Fenics forms for the energies
def sigma(u, alpha):
    return (a(alpha) + k_ell) * sigma_0(u)


elastic_energy = 0.5 * inner(sigma(u, alpha), eps(u)) * dx
external_work = dot(body_force, u) * dx
#Commented by Vahid
#dissipated_energy = Gc / float(c_w) * (w(alpha) / ell + 0.5 * ell * dot(grad(alpha), grad(alpha))) * dx(domain=mesh)
#dissipated_energy = Gc / float(c_w) * (w(alpha) / ell + 0.5 * ell * dot(grad(alpha), grad(alpha))) * dx(domain=mesh)
#Added by Vahid for own phase field functional
dissipated_energy = Gc * 0.5 * ((w(alpha) * w(alpha)) / ell + ell * dot(grad(alpha), grad(alpha))) * dx
#Commented by Vahid
total_energy = elastic_energy + dissipated_energy - external_work
#Added by Vahid
#total_energy = elastic_energy + dissipated_energy + energy_vol - external_work

# Derivatives of the energy
#Added by Vahid
E_up = derivative(total_energy, up, vp)

E_u = derivative(total_energy, u, v)
E_alpha = derivative(total_energy, alpha, beta)
E_alpha_alpha = derivative(E_alpha, alpha, dalpha)

u, du, v = Function(V_u), TrialFunction(V_u), TestFunction(V_u)
#Added by Vahid
up, dup, vp = Function(V_up), TrialFunction(V_up), TestFunction(V_up)

# Variational problem for the displacement
E_du = replace(E_u, {u: du})  # Writing tangent problems in term of test and trial functions for matrix assembly
#Added by Vahid
E_dup = replace(E_up, {up: dup})  # Writing tangent problems in term of test and trial functions for matrix assembly
#problem_up = LinearVariationalProblem(lhs(E_dup), rhs(E_dup), up, bcs=bc_u)

# Residual_up = derivative(total_energy, up, vp)
# Jacobian_up = derivative(Residual_up, up, dup)
# problem_up = LinearVariationalProblem(Jacobian_up, Residual_up, up, bcs=bc_u)
# solver_up = LinearVariationalSolver(problem_up)
# solver_up.parameters.update(solver_u_parameters)
# solver_up.solve()

problem_u = LinearVariationalProblem(lhs(E_du), rhs(E_du), u, bc_u)

#Added by Vahid for initial phase field
class MyExpression(Expression):
    def eval(self, value, x):
        #Commented by Vahid for test
        dx = x[0] - 0.5
        #Added by Vahid for test: two lines
        # dx = x[0] - 0.25
        # dz = x[0] - 0.75
        dy = x[1] - 0.5
        ell = ellv
        if dx > 0.0:
        # Added by Vahid for test: two lines
        # if dx < 0.0 or dz > 0.0:
        #     dx = min(abs(dx), abs(dz))
            value[0] = exp(-sqrt(dx * dx + dy * dy) / ell)
        else:
            value[0] = exp(-abs(dy) / ell)

            def value_shape(self):
                return (1,)


fd = MyExpression(degree=1)
alpha_0 = interpolate(fd, V_alpha)
alpha.assign(alpha_0)

# Variational problem for the damage (non-linear to use variational inequality solvers of petsc)
class DamageProblem(OptimisationProblem):
    def f(self, x):
        return assemble(total_energy)

    def F(self, b, x):
        pass

    def J(self, A, x):
        pass

    def form(self, A, b, x):
        # alpha.vector()[:] = x
        assemble_system(E_alpha_alpha, E_alpha, bc_alpha, A_tensor=A, b_tensor=b)


problem_alpha = DamageProblem()

# Set up the solvers

solver_u = LinearVariationalSolver(problem_u)
solver_u.parameters.update(solver_u_parameters)
# info(solver_u.parameters, True)
solver_alpha = PETScTAOSolver()
solver_alpha.parameters.update(solver_alpha_parameters)
lb = interpolate(Expression("0."), V_alpha)  # lower bound, set to 0
ub = interpolate(Expression("1."), V_alpha)  # upper bound, set to 1
# info(solver_alpha.parameters,True) # uncomment to see available parameters

#  initialization of structures to store datas
errors = [];
alpha_max = [];
file_alpha = File(savedir + "/alpha.xdmf")  # use .pvd if .xdmf in not working
file_u = File(savedir + "/u.xdmf")  # use .pvd if .xdmf in not working

# Alternate mininimization
# Initialization
iter = 1;
err_alpha = 1

#Commented by Vahid for initializing phase field
#alpha_0 = interpolate(Expression("0.0"), V_alpha)  # initial (known) alpha

# Iterations
while err_alpha > toll and iter < maxiter:
    # solve elastic problem
    solver_u.solve()
    # solve damage problem
    solver_alpha.solve(problem_alpha, alpha.vector(), lb.vector(), ub.vector())
    # test error
    alphadiff = alpha.vector().array() - alpha_0.vector().array()
    err_alpha = np.linalg.norm(alphadiff, ord=np.Inf)
    # monitor the results
    if mpi_comm_world().rank == 0:
        print "Iteration:  %2d, Error: %2.8s, alpha_max: %.8g" % (iter, err_alpha, alpha.vector().max())
    # update iteration
    alpha_0.assign(alpha)
    iter = iter + 1
    # plot the damage fied
    plot(alpha)
    # Save the field at each iteration
    file_alpha << (alpha)
    file_u << (u)
    # save the error
    errors.append(err_alpha)
    alpha_max.append(alpha.vector().max())

# Calculate the energies
elastic_energy_value = assemble(elastic_energy)
surface_energy_value = assemble(dissipated_energy)
if mpi_comm_world().rank == 0:
    print "-----------------------------------------"
    print("\nElastic and surface energies: (%g,%g)" % (elastic_energy_value, surface_energy_value))
    print "-----------------------------------------"

# Save some global quantities as a function of the time
# np.savetxt(savedir+'/errors.txt', iterations)

# Plot the damage field and the mesh
plot(alpha)
list_timings()

# Plot energy and stresses
import matplotlib.pyplot as plt


def plot_errors():
    plt.semilogy(errors[:], 'b-o', linewidth=2)
    plt.xlabel('Iteration')
    plt.ylabel('Error')


def plot_alpha_max():
    plt.plot(alpha_max[:], 'b-o', linewidth=2)
    plt.xlabel('Iteration')
    plt.ylabel('alpha_max')


def plot_errors_alpha_max():
    plt.subplot(211)
    plot_errors()
    plt.subplot(212)
    plot_alpha_max()
    plt.savefig(savedir + '/iterations.png')
    plt.show()


# plot the errors
plot_errors_alpha_max()
# make plots interactive
interactive()