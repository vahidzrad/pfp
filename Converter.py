from dolfin import *
import os
import subprocess

# I create a tempFile
directoryNew=os.getcwd()+'/Mesher'
if not os.path.exists(directoryNew):
        os.makedirs(directoryNew)

subprocess.call(['dolfin-convert', '-i', 'gmsh', 'meshpfp.msh', 'meshSingle.xml'],cwd=directoryNew)

# I/O Dolfin.
mesh = Mesh("Mesher/meshSingle.xml")
subdomains = MeshFunction("size_t", mesh, "Mesher/meshSingle_physical_region.xml")
boundaries = MeshFunction("size_t", mesh, "Mesher/meshSingle_facet_region.xml")
mesh_file = XDMFFile(mesh.mpi_comm(), "Mesher/meshSingle.xdmf")
mesh_file.write(mesh)


dxM = Measure("dx")(domain=mesh,subdomain_data=subdomains)
# for integrate on external facet
dsM = Measure("ds")(domain=mesh,subdomain_data=boundaries)
#for integrate on internal facet (interface facet are considered internal)
#but one needs to take account the orientation of normal
dSM = Measure("dS")(domain=mesh,subdomain_data=boundaries)
V = FunctionSpace(mesh, "Lagrange", 1)
u= TrialFunction(V)
v= TestFunction(V)
# bilinear form on each subdomains
a0 = inner(grad(u),grad(v))*dxM(1)
a1 = inner(grad(u),grad(v))*dxM(2)
# as well you can do for the integration on external facet


