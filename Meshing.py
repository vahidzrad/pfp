from __future__  import print_function
import pytest
import pygmsh as gp
import numpy  as np
import random
# to write on a temp file
import os
import tempfile
import subprocess
# Define a Curve Mesh in pygmsh

h_i=0.05
Point1=np.array([0.,0.,0.])
Point2=np.array([1.,0.,0.])

Point3=np.array([1.,0.5,0.])
Point4=np.array([0.,0.5,0.])

Point5=np.array([0.,1.,0.])
Point6=np.array([1.,1.,0.])

Domain1=gp.Geometry()
Pt1=Domain1.add_point(Point1,h_i) # Id 0
Pt2=Domain1.add_point(Point2,h_i) # Id 1
Pt3=Domain1.add_point(Point3,h_i)
Pt4=Domain1.add_point(Point4,h_i)




h_ii=h_i
# In this case we have a matching interface, otherwise put h_ii
Pt5=Domain1.add_point(Point5,h_ii)
Pt6=Domain1.add_point(Point6,h_ii)


# Add line 1-2, 3-4 and 4-1
Line1=Domain1.add_line(Pt1,Pt2)
Line2=Domain1.add_line(Pt2,Pt3)
Line3=Domain1.add_line(Pt3,Pt4)
Line4=Domain1.add_line(Pt4,Pt1)


#Line2Double=Domain1.add_line(Pt3Double,Pt2Double)

Line5=Domain1.add_line(Pt4,Pt5)
Line6=Domain1.add_line(Pt5,Pt6)
Line7=Domain1.add_line(Pt6,Pt3)



# Line Loop
ListLine1=[Line1,Line2,Line3,Line4]
LineLoop1=Domain1.add_line_loop(ListLine1)


ListLine2=[Line3,Line5,Line6,Line7]
LineLoop2=Domain1.add_line_loop(ListLine2)

# Ruled Surface
SurfaceRuled1=Domain1.add_ruled_surface(LineLoop1)
SurfaceRuled2=Domain1.add_ruled_surface(LineLoop2)

# Physical label
Domain1.add_physical_surface(SurfaceRuled1,'domain1')
Domain1.add_physical_line(Line1,'bottom 1')
Domain1.add_physical_line(Line2,'interface 1')
Domain1.add_physical_line(Line3,'top 1')
Domain1.add_physical_line(Line4,'inflow 1')
Domain1.add_physical_surface(SurfaceRuled2,'domain2')
Domain1.add_physical_line(Line5,'bottom 2')
Domain1.add_physical_line(Line6,'outflow')
Domain1.add_physical_line(Line7,'top')

List_of_customString=['Mesh.ElementOrder=1;','Mesh.Optimize=1;']
Domain1.add_raw_code(List_of_customString)
Domain1.get_code()


# I create a tempFile
directoryNew=os.getcwd()+'/Mesher'
if not os.path.exists(directoryNew):
        os.makedirs(directoryNew)
handle, filename = tempfile.mkstemp(prefix='Mesh1',suffix='.geo',dir=directoryNew)
os.write(handle, Domain1.get_code())
os.close(handle)
# Generate the mesh
#subprocess.call(["gmsh", "-2", "-o", "meshBlock.msh", filename],cwd=directoryNew)
#subprocess.call(['dolfin-convert', '-i', 'gmsh', 'meshBlock.msh', 'meshBlock.xml'],cwd=directoryNew)

# to be sure of correct order of call
# use ipython and do
# command_line=raw_input()
# import shlex
# args=shlex.split(command_line)
# print(args)
